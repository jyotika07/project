<!DOCTYPE html>
<html>
<head>
	<title>php learning</title>
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
</head>
<body>
	<div class="container-fluid">
<nav class="navbar navbar-expand-lg navbar-light bg-light ">
  <a class="navbar-brand mt-4" href="#">User Management System</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav mt-4">
      <a class="nav-item nav-link active ml-4" href="#">Admin Panel </a>
      <a class="nav-item nav-link ml-4" href="#">Pages Management</a>
      <a class="nav-item nav-link ml-4" href="#">Logout</a>
    </div>
  </div>
</nav>