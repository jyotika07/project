<?php
include 'db/connection.php';
$i=0;
$id=$_GET['id'];

echo $id;
$pdo = Database::connect();
$getuser = $pdo->prepare('SELECT * FROM users Where id ='.$id);
$getuser->execute();
if($getuser->rowCount() > 0) {
  $row=$getuser->fetch();
  	$name=$row['name'];
  	$id=$row['id'];
  	$username=$row['username'];
  	$email=$row['email'];
  	$brand=$row['brand'];

  }
?>
<div class="container">
	<h2 class="border text-center">Product Detail</h2>
	<div class="row mt-4">
		<div class="col-6 ">
			<img src=<?php echo $paths; ?> alt="image not found" width="100%">
		</div>
		<div class="col-6  ">
			<div class="row mt-5">
				<div class="col-6">
					<h5 class="text-dark">Product Name:
					</div>
					<div class="col-6 m">	
						<h5 ><?php echo $name; ?></h5>
					</div>
					<div class="col-6">
					<h5 class="text-dark">Product Brand:
					</div>
					<div class="col-6">	
						<h5 ><?php echo $brand; ?></h5>
					</div>
					<div class="col-6">
					<h5 class="text-dark">Product Price:
					</div>
					<div class="col-6">	
						<h5 ><?php echo $price; ?></h5>
					</div>
					<div class="col-6">
					<h5 class="text-dark">Total Price:
					</div>
					<div class="col-6">	
						<h5 ><?php echo $price; ?></h5>
					</div>
						<div class="col-12 pl-5 pr-5 text-white mt-5"><button type="button" class="btn btn-dark btn-lg btn-block"><a href="checkout.php">Buy Now</a></button></div>
				</div>
			</div>
		</div>
	</div>
	
