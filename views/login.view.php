    <form method="post" class="float-center bg-light" action="/admin">
        <div class="container-fluid py-3">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card card-body">
                        <h3 class="text-center mb-4">Login</h3>
                        <fieldset>
                            <div class="form-group has-error">
                                <h5>username</h5>
                                <input class="form-control input-lg" placeholder="User name" name="uname" type="text">
                            </div>
                          
                            <div class="form-group has-success">
                                <h5>Password</h5>
                                <input class="form-control input-lg" placeholder="Password" name="pass" value="" type="password">
                            </div>
                         
                            <input class="btn btn-lg btn-primary btn-block button" value="login" name="submit" type="submit" >
                             <div class="text-center mt-4">
                            <a href="/signup" id="">Create New Account</a> 
                        </fieldset>             
                    </div>
                </div>
            </div>
        </div>
    </form>
