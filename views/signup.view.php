<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link rel="stylesheet" href="../css/bootstrap.min.css">
     <link rel="stylesheet" href="css/style.css">
</head>
<body >


    <form method="post" class="float-center bg-light" action="/insert">
        <div class="container-fluid py-3">
            <div class="row">
                <div class="col-md-6 mx-auto">
                    <div class="card card-body">
                        <h3 class="text-center mb-4">signup</h3>
                        <fieldset>
                            <div class="form-group has-error">
                                <h5>name</h5>
                                <input class="form-control input-lg" placeholder=" name" name="name" type="text">
                            </div>
                            <div class="form-group has-error">
                                <h5>username</h5>
                                <input class="form-control input-lg" placeholder="User name" name="username" type="text">
                            </div>
                            <div class="form-group has-error">
                                <h5>email</h5>
                                <input class="form-control input-lg" placeholder="email" name="email" type="text">
                            </div>
                            <div class="form-group has-success">
                                <h5>Password</h5>
                                <input class="form-control input-lg" placeholder="Password" name="password" value="" type="password">
                            </div>
                         
                            <input class="btn btn-lg btn-primary btn-block button" value="signup" name="submit" type="submit" >
                            <div class="text-center">
                                IF Account Already Exist
                            <a href="/login" id="">Login</a> 
                        </div>
                        </fieldset>
                       

                        
                    </div>
                </div>
            </div>
        </div>
    </form>
