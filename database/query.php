<?php 

class query {

	public $pdo;

	public function __construct($pdo) {

		$this->pdo=$pdo;
	}
 //select data 
	public function select($table) {

	 	$statement=$this->pdo->prepare("select * from {$table}");	
		$statement->execute();
		return $statement;
	}


	public function insert($table,$parameters) {


		$sql=sprintf(
			'insert into %s (%s) values (%s)',
			 $table,
			 implode(',',array_keys($parameters)),
			 ':'.implode(', :',array_keys($parameters))
		);

		$statement=$this->pdo->prepare($sql);
		$statement->execute($parameters);
	}

	public function delete($table,$id) {

	 $statement = $this->pdo->prepare("delete from {$table} WHERE id = '$id'");
       
      $statement->execute();
      return statement;

	
	}
}

 ?>